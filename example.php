<?php
$location = realpath(dirname(__FILE__));
require_once $location . '/binary_representation_to_data_v1.php';
$return = binary_representation_to_data_v1('010101000110100001101001011100110010000001101001011100110010000001100101011110000110000101101101011100000110110001100101001000000110010001100001011101000110000100101110', TRUE);
if ($return === FALSE){
	echo "FALSE\n";
} else {
	echo "{$return}\n";
}
?>