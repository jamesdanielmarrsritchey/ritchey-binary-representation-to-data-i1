<?php
#Name:Binary Representation To Data v1
#Description:Convert binary representation (base2) of data back to data. Returns the representation as a string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'string' (required) is a string containing the binary representation to convert. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,display_errors:bool:optional
#Content:
if (function_exists('binary_representation_to_data_v1') === FALSE){
function binary_representation_to_data_v1($string, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@preg_match('/[(1)(0)]/', $string) === FALSE){
		$errors[] = 'string';
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;	
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		$string = @str_split($string, 8);
		foreach ($string as &$value){
			$value = pack('H*', base_convert($value, 2, 16));
		}
		$binary = @implode($string);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('binary_representation_to_data_v1_format_error') === FALSE){
			function binary_representation_to_data_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("binary_representation_to_data_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $binary;
	} else {
		return FALSE;
	}
}
}
?>